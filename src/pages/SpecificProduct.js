import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Navigate, Link, useNavigate } from 'react-router-dom';
//useParams() -> contains any values we are trying to pass in the URL stored
//useParams is how we receive the courseId passed via the URL

export default function SpecificProduct(){
	const navigate = useNavigate();
	const { productId } = useParams();
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(1);
	useEffect(() => {
		fetch(`https://passion-inside.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])
	const { user } = useContext(UserContext);
	const decrement = () => {
		setQuantity(prevCount => prevCount - 1)
	}
	const increment = () => {
		setQuantity(prevCount => prevCount + 1)
	}


	//checkout function
	const createOrder = (productId) => {
		fetch('https://passion-inside.herokuapp.com/users/create-order', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: `You have successfully added ${name} to your cart`
				})
				// navigate('/products')
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
			}
		})
	}
	return(
		<Container>
		<p className="ms-1 font-italic"><Link to="/products">Products</Link>/{name}</p>
			<Card>
				<Card.Header>
					<h4>{ name }</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{ description }</Card.Text>
					<h6>Price: Php { price }</h6>
				</Card.Body>
				<h6 className="container-fluid ms-1">Quantity:</h6>
				<div className="row container-fluid mb-3">
					<Button variant="secondary" type="button" className="input-group-text" onClick={decrement} style={{ width: '2rem' }}>-</Button>
					<div style={{ width: '3rem' }} className="form-control text-center">{quantity}</div>
					<Button variant="secondary"style={{ width: '2rem' }} type="button" className="input-group-text" onClick={increment}>+</Button>
				</div>
				<Card.Footer>
				{user.accessToken !== null ?
					<Button variant='primary' onClick={() => createOrder(productId)}>Order</Button>
					:
					<Button variant='warning' as={Link} to="/login">Login to Order</Button>
				}
				</Card.Footer>
			</Card>
			<Button variant="warning" className="buttonOrderHistory mb-5 mt-5 container-fluid d-flex justify-content-center" style={{ width: '10rem' }} as={ Link } to={`/checkout`}>Checkout</Button>
		</Container>
		)
}