import { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import CheckoutOrder from './pages/CheckoutOrder'
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ProductPage from './pages/ProductPage';
import SpecificProduct from './pages/SpecificProduct'
import { Container, Card } from 'react-bootstrap';
import { UserProvider } from './UserContext';

//react-router
import { BrowserRouter, Routes, Route } from 'react-router-dom';


function App() {

  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  //function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value = {{ user, setUser, unsetUser }}>
      <BrowserRouter>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={ <Home /> } />
            <Route path="/products" element={ <ProductPage /> } />
            <Route path="/register" element={ <Register /> } />
            <Route path="/login" element={ <Login /> } />
            <Route path="/logout" element={ <Logout /> } />
            <Route path="/checkout" element={ <CheckoutOrder /> } />
            <Route path="/products/:productId" element={ <SpecificProduct /> } />
          </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
