import { useState, useEffect, useContext} from 'react';
//React bootstrap components
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar  expand="lg" variant="light" className="mb-5 navBar">
			<Navbar.Brand className="ms-3">Passion Inside</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse>
				<Nav className="ms-auto nav">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					
					<Nav.Link as={Link} to="/products">Products</Nav.Link>
					{(user.accessToken !== null)?
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						:
					<>
						<Nav.Link as={Link} to="/register">Register</Nav.Link>
						<Nav.Link as={Link} to="/login">Login</Nav.Link>
					</>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}
