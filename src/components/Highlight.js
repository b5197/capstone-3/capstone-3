import { Row, Col, Card, Image, Button } from 'react-bootstrap';




export default function Highlights(){
	const cardInfo = [
		{image: "https://picsum.photos/id/258/200", title: "Pigeon Supplements", text: "For now, we only have Excellence Rockdove/Gamefowl products to offer. But we will soon add other leading pigeon supplements to satisfy your needs."},
		{image: "https://picsum.photos/id/258/200", title: "Pigeon Supplies", text: "We are also the pioneer in making boxes for pigeons. We call it Passion Inside Pigeon Boxes. You can have it at your own preference/personalize or have it with just the original design."},
		{image: "https://picsum.photos/id/258/200", title: "Pigeon Fanciers", text: "We love to here any of your suggestions. If you have anything in mind, comments, suggestions, or any inputs, feel free to message our Facebook page Passion Inside."}
	]
	
	const renderCard = (card, index) => {
		return(
				<Card style={{ width: '25rem' }} className="cardHighlight p-3">
				  <Card.Img variant="top" src={card.image} />
				  <Card.Body>
				    <Card.Title>{card.title}</Card.Title>
				    <Card.Text>{card.text}</Card.Text>
				  </Card.Body>
				</Card>
			)
	}
return(
	<div className="container-fluid d-flex justify-content-evenly text-center cardBottom">{cardInfo.map(renderCard)}</div>
	)
}


