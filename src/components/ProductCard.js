import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, name, description, price } = productProp;

	return(
	<Card className="mt-3 mb-5" >
				<Card.Body>
					<Card.Title> { name } </Card.Title>

					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text> { description } </Card.Text>

					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php  { price } </Card.Text>
					

				
					<Button variant="secondary" as={ Link } to={`/products/${_id}`}>Details</Button>
				</Card.Body>
	</Card>
	)
}


ProductCard.propTypes = {
	//shape method is used to check if a prop object has the same specific shape of data types
	productProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})

}






