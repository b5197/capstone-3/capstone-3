//React bootstrap components
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
	return (
		<Row>
			<Col className="p-5">

				<h1 className="mb-3 h1PassionInside">Passion Inside</h1>
				<p className="mb-3">Pigeon racing product supplies and supplements</p>
				<Button variant="secondary" as={Link} to="/products">Order now!</Button>
			</Col>
		</Row>


		)
}
