import { useState, useEffect } from 'react';
import ProductCard from './ProductCard'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';


export default function	UserView({productsData}) {

	const[products, setProducts] = useState([])

	//we write our .map inside the useEffect to render the rapid changes of the data.
	useEffect(() => {
		const productsArr = productsData.map(product => {
			//only render the active products
			if(product.isActive === true) {
				return(
					<ProductCard key={product._id} productProp={product}/>
					)
			} else {
				return null
			}
		})
		//set the products state to the result of our map function to bring our return product component outside of the scope of our useEffect where our return statement below can see
		setProducts(productsArr)
	}, [productsData])

	return(
		<>
			{ products }

		</>
		)
}  